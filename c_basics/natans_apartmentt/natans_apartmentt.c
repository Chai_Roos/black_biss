#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int *findindexofchar(char array[3][3], char c){ // finds the index of char in coder
    int *index = (int*)malloc(sizeof(int)*2);
    if (c == '0'){
        index[0] = -1;
        index[1] = -1;
        return index;
    }
    int i, j;
    for(i = 0; i < 3; i++){
        for (j = 0; j < 3; j++){
            if (array[i][j] == c){
                index[0] = i;
                index[1] = j;
                return index;
            }
        }
    }
    index[0] = -2;
    index[1] = -2;
    return index;
}

char *optionsofchar(char array[3][3], char c){ // returns an array of options for character
    int tracker = 0;
    char *options = (char*)malloc(sizeof(char)*5);
    int *index = findindexofchar(array, c);
    options[tracker] = c;
    tracker++;
    if (c == '0'){
        options[tracker] = '8';
        tracker++;
    }
    else{
        if (c == '8'){
            options[tracker] = '0';
            tracker++;
        }
        if (index[0] - 1 >= 0){ /* up */
            options[tracker] = array[index[0] - 1][index[1]];
            tracker++;
        }
        if (index[1] - 1 >= 0){ /* left */
            options[tracker] = array[index[0]][index[1] - 1];
            tracker++;
        }
        if (index[0] + 1 < 3){ /* down */
            options[tracker] = array[index[0] + 1][index[1]];
            tracker++;
        }
        if (index[1] + 1 < 3){ /* right */
            options[tracker] = array[index[0]][index[1] + 1];
            tracker++;
        }
    }
    char *options1 = realloc(options, (size_t)tracker);
    free(index);
    return options1;
}

int length_of_final(char **coder, char* string, int counter){ // returns the length of the final array that includes all the options

    if (string[counter] == '\0'){
        return 1;
    }
    return strlen(optionsofchar(coder, string[counter])) * length_of_final(coder, string, counter + 1);
}

char **natans_apartment(char **answers, char **options_for_every_char, int *indexes, int length_of_code, int counter){
    if (counter == 0){
        return answers;
    }
    else{
        int nextloop = 0;
        for (int y = 0; y < length_of_code; y++){
            if (indexes[y] == strlen(options_for_every_char[y]) && y != 0){
                indexes[y] = 0; indexes[y - 1]++;
                nextloop = 1;
                break;
            }
        }
        if(nextloop == 1){
            natans_apartment(answers, options_for_every_char, indexes, length_of_code, counter);
        }
        else{
            for (int i = 0; i < length_of_code; i++){
                strncat(answers[counter - 1], &options_for_every_char[i][indexes[i]], 1);
            }
            indexes[length_of_code - 1]++;
            natans_apartment(answers, options_for_every_char, indexes, length_of_code, counter - 1);
        }
    }
}

int main()
{
    char coder[3][3] = {
                            "123",
                            "456",
                            "789"
                        };
    char code[] = "3181";
    int all_the_options_length = length_of_final(coder, code, 0);
    char **answers;
    answers = malloc(all_the_options_length * sizeof(char *));
    for (int i = 0; i < all_the_options_length; i++){
        answers[i] = (char*)calloc((strlen(code)), sizeof(char));
    }
    char* option_for_each[strlen(code)];
    for (int k = 0; k < strlen(code); k++){
        option_for_each[k] = optionsofchar(coder, code[k]);
    }
    int indexes[strlen(code)];
    for (int z = 0; z < strlen(code); z++){
        indexes[z] = 0;
    }

    answers = natans_apartment(answers, option_for_each, indexes, strlen(code), all_the_options_length);

    for(int i = 0; i < all_the_options_length; i++){
        printf("%s \n", answers[i]);
    }
    for(int i = 0; i < all_the_options_length; i++){
        free(answers[i]);
    }
    free(answers);
    return(0);
}
