// A simple C program for traversal of a linked list
#include <stdio.h>
#include <stdlib.h>

struct Node {
    struct Node* next;
};


int getCount(struct Node* head)
{
    int count = 0;  // Initialize count
    struct Node* current = head;  // Initialize current
    while (current != NULL)
    {
        count++;
        current = current->next;
    }
    return count;
}

// This function prints contents of linked list starting from
// the given node
void insert_n(struct Node* list, int N) {
    if (N < 1 || N > getCount(list) + 1){
        printf("out of list borders\n");
        return;
    }
    struct Node* new_ = NULL;
    new_ = (struct Node*)malloc(sizeof(struct Node));

    struct Node* temp0 = list;
    struct Node* temp = list;

    if (N == 1){
        list = new_;
        new_->next = temp;
    }
    else {
        for(int i = 0; i < N - 2; i++) {
            temp = temp->next;
            temp0 = temp0->next;
        }
        temp = temp->next;
        temp0->next = new_;
        new_->next = temp;
    }
}

int main()
{
    struct Node* head = NULL;
    struct Node* second = NULL;
    struct Node* third = NULL;

    // allocate 3 nodes in the heap
    head = (struct Node*)malloc(sizeof(struct Node));
    second = (struct Node*)malloc(sizeof(struct Node));
    third = (struct Node*)malloc(sizeof(struct Node));

    head->next = second; // Link first node with second

    second->next = third;

    third->next = NULL;

    insert_n(head, -1);

    return 0;
}
