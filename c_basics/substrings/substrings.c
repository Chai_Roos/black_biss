/**
 * @file substrings.c
 * @version 1.0
 * @author ????????
 *
 * @brief program that check if one string is substring of the other and return pointer
 * to the start of the substring in the string.
 *
 * @section LICENSE
 * This program is part of the Black-Biss;
 * ANY USE OF THE PROGRAM OR THE SOURCE CODE WITHOUT THE BLACK-BISS AGREEMENT IS FORBIDDEN.
 *
 */

// ------------------------------ includes ------------------------------
#include <stdio.h>
#include <stdlib.h>

// ------------------------------ functions -----------------------------
char * my_strstr(const char * a, const char * b)
{

    if (strlen(a) > strlen(b)){
        int current_short = 0;
        for (int i = 0; i < strlen(a); i++){
            if (current_short == strlen(b) - 1){
                return b;
            }
            if (a[i] == b[current_short]){
                    current_short++;
            }
        }
    }
    else {
        int current_short = 0;
        for (int i = 0; i < strlen(b); i++){
            if (current_short == strlen(a) - 1){
                return a;
            }
            if (b[i] == a[current_short]){
                    current_short++;
            }
        }
    }
    return NULL;
}

int main(int argc, char * argv[])
{
   char s1[] = "AAAAAAACCCAAAAAAAA";
   char s2[] = "CCC";

   char * ret = my_strstr(s1, s2);

   printf("The substring is: %s\n", ret);

	return 0;
}
