"""
Black Biss - Advance Python

Write a function that get a path to file that encrypted with 3 english-small-letter xor key, and print the
deciphered text.
"""
import copy

def crypto_breaker(file_path):
    file = open(file_path, "r")
    list_of_crypto = file.read().split(",")
    random_now = 0
    final_keys = [[],[],[]]
    while random_now < 3:
        try_ = 97 # first random --> "a"
        pointer = random_now
        while try_ <= 122:
            this_try = True # success
            while pointer < len(list_of_crypto) - 1:
                first_condition = int(list_of_crypto[pointer]) ^ try_ > 31
                second_condition = int(list_of_crypto[pointer]) ^ try_ <= 126
                if not (first_condition and second_condition):
                    if not (int(list_of_crypto[pointer]) ^ try_ == 10):
                        this_try = False
                        break
                pointer += 3
            if this_try == True:
                final_keys[random_now].append(try_)
            pointer = random_now
            try_ += 1
        random_now += 1
    value = 0
    while(value < 3):
        random_now = final_keys[value][0]
        if (value == 1):
            random_now = final_keys[value][0]
        pointer = copy.deepcopy(value)
        while pointer < len(list_of_crypto) - 1:
            list_of_crypto[pointer] = chr(int(list_of_crypto[pointer]) ^ random_now)
            pointer += 3
        value += 1 # moves to the next key
    
    for crypt in list_of_crypto:
        print (crypt, end="")


    file.close()


# small test to check it's work.
if __name__ == '__main__':
    crypto_breaker("big_secret.txt")




# print (int(bin(ord("x"))[-7:]) ^ int(bin(ord("a"))[-7:]))
# print (85 ^ 182)