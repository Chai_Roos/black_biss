"""
Black Biss - Advance Python

Write a function that read one line from the user with many potentional passwords seperated by comma ","
and print out only the valid password - by the following rules:
    * contain letter from each of the characters-sets:
        lower-case english, upper-case english, decimal digit,
        special characters *&@#$%^
    * dosn't contain other symbols (ignore white-spaces)
    * length between 6 and 12

"""


def is_special_character(char):
	special_options = ["@","#","$","%","^","&","*"]
	for option in special_options:
		if char == option:
			return True
	return False

def validate_passwords():
    passwords = input("Enter passwords  ").split(",")
    for password in passwords:
    	current_password = password.strip()
    	lower_case_letter = False
    	upper_case_letter = False
    	number = False
    	special_char = False
    	char_counter = 0
    	for char in current_password:
    		char_counter += 1
    		if (char_counter > 12):
    			break
    		if char.isalpha() and char == char.lower():
    			lower_case_letter = True
    		elif char.isalpha() and char == char.upper():
    			upper_case_letter = True
    		elif char.isdigit():
    			number = True
    		elif is_special_character(char):
    			special_char = True
    	if char_counter > 5 and lower_case_letter and upper_case_letter and number and special_char:
    		print (password, end=", ")


# small test to check it's work.
if __name__ == '__main__':
    validate_passwords()


"""
just found out after finishing the exercise about the rejex module
the code works anyhow so I'm not changing it. but I just wanted to note that I'm familiar with rejex (now at least...)
"""