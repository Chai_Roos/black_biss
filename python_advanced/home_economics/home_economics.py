"""
Black Biss - Advance Python

Write a function that calculate how many month one should save money to buy new car.
"""

def home_economics(startPriceOld, startPriceNew, savingPerMonth, percentLossByMonth):
    money_earned = 0
    monthes_counter = 0
    while startPriceNew > startPriceOld + money_earned:
    	monthes_counter += 1
    	# beginning of month
    	if monthes_counter % 2 == 0:
    		percentLossByMonth += 0.5
    	# end of month
    	money_earned += savingPerMonth # has to be every end of month
    	startPriceNew = startPriceNew * ((100.0 - float(percentLossByMonth)) / 100 )
    	startPriceOld = startPriceOld * ((100.0 - float(percentLossByMonth)) / 100 )
    return [monthes_counter, round(startPriceOld + money_earned - startPriceNew)]

# small test to check it's work.
def test():
    ret = home_economics(2000, 8000, 1000, 1.5)
    if ret[0] == 6 and ret[1] == 766:
        print("HAZA! simple test pass")
    else:
        print("Oops, it's not working yet")

test()