"""
Black Biss - Advance Python

Write a function that calculate how far is our ejected pilot.
Run this program and insert the following lines (each end with Enter):

UP 5
DOWN 3
LEFT 3
RIGHT 2
0

and the result should be 2
"""
import math

def ejected():
    height = 0
    sides = 0
    input_ = input()
    while input_ != "0" and len(input_.split()) == 2:
        input_meaning = input_.split()
        direction = input_meaning[0]
        num_steps = input_meaning[1]
        if direction == "UP":
            height += int(num_steps)
        elif direction == "DOWN":
            height -= int(num_steps)
        elif direction == "LEFT":
            sides -= int(num_steps)
        elif direction == "RIGHT":
            sides += int(num_steps)
        input_ = input()
    return round(math.sqrt(math.fabs(height) ** 2 + math.fabs(sides) ** 2))


# small test to check it's work.
def text():
    print(ejected())

text()