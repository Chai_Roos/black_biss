"""
Black Biss - Advance Python

Write a function that suggest which word is the Pirates meant to write.
"""


def dejumble(word, potentional_words):
    my_word = sorted(list(word))
    good_words = []
    for current_word in potentional_words:
    	if sorted(list(current_word)) == my_word:
    		good_words.append(current_word)
    return good_words


# small test to check it's work.
if __name__ == '__main__':

    ret = dejumble("orspt", ["sport", "parrot", "ports", "matey"])
    if len(ret) == 2 and set(ret) == set(["sport", "ports"]):
        print("HAZA! simple test pass")
    else:
        print("Oops, it's not working yet")