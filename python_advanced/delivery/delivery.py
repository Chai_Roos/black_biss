"""
Black Biss - Advance Python

Write a function that get array of string, each represent a row in 2-dim map, when:
    'X' is start / destination point
    '-' is road right-left
    '|' is road up-down
    '+' is a turn in the road, can be any turn (which isn't continue straight)
    path = ["           ",
            "X-----+    ",
            "      |    ",
            "      +---X",
            "           "]

    # Note: this grid is only valid when starting on the right-hand X, but still considered valid
    path = ["                      ",
            "   +-------+          ",
            "   |      +++---+     ",
            "X--+      +-+   X     "]
    nevigate_validate(path)  # ---> True
"""

import copy

def find_x(path, list_of_already_tried):
    for index, line in enumerate(path):
        for inner_index, char in enumerate(line):
            if char == "X":
                if [index, inner_index] not in list_of_already_tried:
                    list_of_already_tried.append([index, inner_index])
                    return [[index, inner_index], list_of_already_tried]
    return [[-1, -1], "nothing special"]

def initial_direction(path, index_start):
    if index_start[0] > 0 and path[index_start[0] - 1][index_start[1]] != " ": # up
        return (1)
    if index_start[0] < len(path) - 1 and path[index_start[0] + 1][index_start[1]] != " ": # down
        return (1)
    if index_start[1] > 0 and path[index_start[0]][index_start[1] - 1] != " ": #left
        return (0)
    if index_start[1] < len(path[0]) and path[index_start[0]][index_start[1] + 1] != " ": # right
        return (0)

def next_one_up(path, current_index, direction, list_of_already_tried):
    if direction == 1:
        ifup = current_index[0] > 0 and (path[current_index[0] - 1][current_index[1]] == "|" or path[current_index[0] - 1][current_index[1]] == "+" or path[current_index[0] - 1][current_index[1]] == "X") # up
        ifdown = current_index[0] < len(path) - 1 and (path[current_index[0] + 1][current_index[1]] == "|" or path[current_index[0] + 1][current_index[1]] == "+" or path[current_index[0] + 1][current_index[1]] == "X") # down
        if ifup and ifdown:
            if path[current_index[0] - 1][current_index[1]] == "X" or path[current_index[0] + 1][current_index[1]] == "X":
                if [current_index[0] - 1, current_index[1]] in list_of_already_tried: # up is starting point
                    return [current_index[0] + 1, current_index[1]]
                elif [current_index[0] + 1, current_index[1]] in list_of_already_tried: # down is starting point
                    return [current_index[0] - 1, current_index[1]]
            else:
                return [-1, -1]
        elif ifup:
            return [current_index[0] - 1, current_index[1]]
        elif ifdown:
            return [current_index[0] + 1, current_index[1]]
        else:
            return [-1, -1]
    elif direction == 0:
        ifleft = False 
        if current_index[1] > 0:
            if (path[current_index[0]][current_index[1] - 1] == "-" or path[current_index[0]][current_index[1] - 1] == "+" or path[current_index[0]][current_index[1] - 1] == "X"): #left
                ifleft = True
        ifright = False
        if current_index[1] < len(path[0]) - 1:
            if (path[current_index[0]][current_index[1] + 1] == "-" or path[current_index[0]][current_index[1] + 1] == "+" or path[current_index[0]][current_index[1] + 1] == "X"): # right
                ifright = True
        if ifleft and ifright:
            if path[current_index[0]][current_index[1] - 1] == "X" or path[current_index[0]][current_index[1] + 1] == "X":
                if [current_index[0], current_index[1] - 1] in list_of_already_tried: # left is starting point 
                    return [current_index[0], current_index[1] + 1]
                elif [current_index[0], current_index[1] + 1] in list_of_already_tried: # right is starting point
                    return [current_index[0], current_index[1] - 1]
            else:
                return [-1, -1]
        elif ifleft:
            return [current_index[0], current_index[1] - 1]
        elif ifright:
            return [current_index[0], current_index[1] + 1]
        else:
            return [-1, -1]

def nevigate_validate(path):
    valid = False
    start_points_checked = []
    while valid == False:
        indexes_of_final_path = []
        try_1_commit = copy.deepcopy(path)
        pointer = find_x(try_1_commit, start_points_checked)[0]
        indexes_of_final_path.append(pointer)
        if pointer == [-1, -1]:
            break
        if next_one_up(try_1_commit, pointer, 0, start_points_checked) != [-1, -1] and next_one_up(try_1_commit, pointer, 1, start_points_checked) != [-1, -1]: # both optional - bad
            break
        elif next_one_up(try_1_commit, pointer, 0, start_points_checked) != [-1, -1]:
            pointer = next_one_up(try_1_commit, pointer, 0, start_points_checked)
        elif next_one_up(try_1_commit, pointer, 1, start_points_checked) != [-1, -1]:
            pointer = next_one_up(try_1_commit, pointer, 1, start_points_checked)
        else:
            break
        indexes_of_final_path.append(pointer)
        direction = initial_direction(try_1_commit, pointer)
        while valid == False:
            current_line = try_1_commit[pointer[0]]
            points_at = try_1_commit[pointer[0]][pointer[1]]
            if points_at == "+":
                try_1_commit[pointer[0]] = current_line[:pointer[1]] + "%" + current_line[pointer[1] + 1:]
                direction = not direction
                pointer = next_one_up(try_1_commit, pointer, direction, start_points_checked)
                if pointer == [-1, -1]:
                    break
                indexes_of_final_path.append(pointer)
            elif points_at == "-":
                try_1_commit[pointer[0]] = current_line[:pointer[1]] + "*" + current_line[pointer[1] + 1:]
                direction = 0
                pointer = next_one_up(try_1_commit, pointer, direction, start_points_checked)
                if pointer == [-1, -1]:
                    break
                indexes_of_final_path.append(pointer)
            elif points_at == "|":
                try_1_commit[pointer[0]] = current_line[:pointer[1]] + "*" + current_line[pointer[1] + 1:]
                direction = 1
                pointer = next_one_up(try_1_commit, pointer, direction, start_points_checked)
                if pointer == [-1, -1]:
                    break
                indexes_of_final_path.append(pointer)
            elif points_at == "X":
                try_1_commit[pointer[0]] = current_line[:pointer[1]] + "$" + current_line[pointer[1] + 1:]
                valid = True
            for line in try_1_commit:
                print (line)
    if valid:    
        for index, line in enumerate(path):
            print(line)
            for inner_index, char in enumerate(line):
                if char != " " and ([index, inner_index] not in indexes_of_final_path):
                    return False
    return valid

# small test to check if it works.
if __name__ == '__main__':
    path = ["           ",
            "X-----+    ",
            "      |    ",
            "      +---X",
            "           "]
    is_valid = nevigate_validate(path)
    path = ["           ",
            "X--|--+    ",
            "      -    ",
            "      +---X",
            "           "]
    invalid = nevigate_validate(path)
    if is_valid and not invalid:
        print("HAZA! simple test pass")
    else:
        print("Oops, it's not working yet")