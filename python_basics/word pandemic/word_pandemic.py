"""
Black Biss - Basic Python

Write a function that replace any second word with the word "Corona".
"""
sentence = input("enter your sentence: ")

def word_pandemic(userInput):
	new_word = "Corona"
	myList = userInput.split() # seperating the words in the sentence to objects in list
	for index, word in enumerate(myList): # running through our new list of words
		if not index % 2 == 0: # activating next action for every second word
			myList[index] = new_word # switch word to be "Corona"
	userInput = "" # deleting initial value of input
	for word in myList: # running through the list of word (which now contains "Corona" every second index)
		userInput += word + " " # inserting the words into a sentence with spaces between them
	userInput = str(userInput).rstrip() # getting rid of last unnecessary space
	return userInput # returning done sentence


#small test to check if it works.
def test():
    base_word = "Koala Bears are the cutest"
    sick_word = "Koala Corona are Corona cutest"

    if word_pandemic(base_word) == sick_word:
        print("HAZA! simple test pass")
    else:
        print("Oops, it's not working yet")

# test()