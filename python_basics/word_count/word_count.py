import sys

def setting_up_file(filename):
	file = open(filename, "r") # opens file for reading
	file = file.read().replace("-", " ").lower() # reading file and replacing dashes with spaces
	bad_characters = ['"', "'", "*", "(", ")", ":", ";","?","!",",","/","|",".","_","`", "[", "]"] # set problematic charaters
	for i in bad_characters: # running on problematic character and for each activating next action
		file = file.replace(i,"") # replacing problematic character with none
	file = sorted(file.split()) # turning the file into a list in which every word is an item, and sorting it
	return file

def print_words(filename):
	file = setting_up_file(filename)
	wordCounter = 0 # going to be used as counter for each word
	for index, word in enumerate(file): # running on the list
		new = False # new stands for: is this word a new one?
		if index == 0: # for the first run, the word will be new
			new = True
		elif word != file[int(index) - 1]: # each word (besides of the first) is going to be checked whether it's similar to the previous word
			new = True # if it is we set new to true
		if new == True: # means 'if the word we're currently on is a new one'
			if not wordCounter == 0: # in order not to print the fist print
				print (wordCounter) # the second argument for each word to be printed
			wordCounter = 0 # restartin counter when starting new word
			print (word, end = " ") # the first argument to be printed
		wordCounter = wordCounter + 1 # adding 1 to counter in any case
	print (wordCounter) # for the last run

	
	

def print_top(filename):
	file = setting_up_file(filename)
	class MyWord: 
		def __init__(self, word, frequency):
			self.word = word
			self.frequency = frequency
	words = [];
	wordCounter = 0 # going to be used as counter for each word
	for index, word in enumerate(file): # running on the list
		new = False # new stands for: is this word a new one?
		if index == 0: # for the first run, the word will be new
			new = True
		elif word != file[int(index) - 1]: # each word (besides of the first) is going to be checked whether it's similar to the previous word
			new = True # if it is we set new to true
		if new == True: # means 'if the word we're currently on is a new one'
			if not wordCounter == 0: # in order not to print the fist print
				w1 = MyWord(file[index-1], wordCounter)
				words.append(w1)
				wordCounter = 0 # restartin counter when starting new word
		wordCounter = wordCounter + 1 # adding 1 to counter in any case
	words = sorted(words, key=lambda word: word.frequency, reverse=True)[:20]
	for currentWord in words:
		print (currentWord.word, " ", currentWord.frequency)





def main():
	if len(sys.argv) != 3:
		print("usage: ./word_count_pt1.py {--count | ---topcount} file")
		sys.exit(1)

	option = sys.argv[1]
	filename = sys.argv[2]
	if option == '--count':
		print_words(filename)
	elif option == '--topcount':
		print_top(filename)

	else:
		print("unknown option: " + option)
		sys.exit(1)


#test
main()