""" exercise optional:
part 1:
"""

def permission1(list1,list2): # function recieve 2 arrays
	if not len(list1) == len(list2): # checking whether or not the length are equally long
		print ("Arrays ate not equally long")
	else:
		tuppleArray = [None] * len(list1) # creating a new array with <arrays length> empty cells
		for index, item in enumerate(list1): # running on one of the list, just for the number of loops necessery
			tuppleArray[index] = (list1[index], list2[index]) # creatin a tuple including necessery items for each cell in new arra
			return tuppleArray # returning finished array

"""
part 2:
"""

def permission2(tupplesArray):
	tupplesArray = sorted(tupplesArray, key=lambda x: x[0]) # sorting by first item of each tuple
	dictionary = {} # creating an empty dictionary
	for item in tupplesArray: # running on tupples array and for each tupple -->
		dictionary[item[0]] = item[1] # inserting key and value to dictionary
	return dictionary


def permission3(list1,list2): # function recieve 2 arrays
	if not len(list1) == len(list2): # checking whether or not the length are equally long
		print ("Arrays ate not equally long")
	else:
		tuppleArray = list(zip(list1, list2)) # using zip function in order to create a tupples array comcluding both lists
		return tuppleArray # returning finished array

def permission3b(tupplesArray):
	tupplesArray = sorted(tupplesArray, key=lambda x: x[0]) # sorting by first item of each tuple
	dictionary = dict(tupplesArray) # using dict method to convert list of tuples to dictionary
	return dictionary

def permission4(list1, list2):
	combinedToTupplesList 	= permission3(list1,list2) # combining 2 lists into one tupples list, when every tupples is (list1[index], list2[index])
	turnedToDictionary 		= permission3b(combinedToTupplesList) # turning list of tupples into dictioary when first item of the tuple in every cell is key and the second is value
	return turnedToDictionary

def permission5(list1, list2):
	theTuppleArray 	= list(zip(list1, list2)) # using zip function in order to create a tupples array comcluding both lists (zip ignores the extra cells in the longer list)
	theDictionary	= permission3b(theTuppleArray) # turning list of tupples into dictioary when first item of the tuple in every cell is key and the second is value
	return theDictionary
"""
for testing:
"""
# experimental = [(1,3),(143,3),(17,3),(73,3),(0,2),(44,32),(11,65),(545,13),(3,4)]
# experimenta2 = [3,5,66,43,2,4,67,4,2,1,11]
# experimenta3 = [3,5,33,22,4,7,53,5,3,2,55]
# print (permission3b(experimental))