"""
Black Biss - Basic Python

Write a function that remove all the odd values in  an array
"""


def odd_ones_out(in_array):
    indexes = [] # creating a list in which I will insert all the indexes of odd numbers from in_array
    for index, item in (enumerate(in_array)): # running through the list 
      	if not item % 2 == 0: # activating next command if current item is odd
       		indexes.insert(0,index) # adding the index of the odd number to the beginning of indexes list
    for place in indexes: # running through indexes list
    	del in_array[place] # deleting from end to begining (important!) the item in our list that matches the index
    return in_array # returning list without odd numbers


# small test to check it's work.
def test():
    arr_base = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
    arr_expected = [0, 2, 4, 6, 8]

    if odd_ones_out(arr_base) == arr_expected:
        print("HAZA! simple test pass")
    else:
        print("Oops, it's not working yet")

# test()