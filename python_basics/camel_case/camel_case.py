def function():
	input_ = input ("enter word:    ")
	list_of_upper = []
	for index, char in enumerate(input_):
		if char.isupper():
			list_of_upper.append(index)
	pointer = len(list_of_upper) - 1
	while pointer >= 0:
		input_ = input_[:list_of_upper[pointer]] + " " + input_[list_of_upper[pointer]:]
		pointer -= 1
	return input_


print (function())