#include <stdio.h>
#include <stdlib.h>
#include <string.h>


int main(int argc, char * argv[])
{
    char memory[1000] = {0};
    int pointer = 0;
    int condition = 0;
    FILE *fptr;
    fptr = fopen("brainfuck.txt", "r");
    if(fptr == NULL)
   {
      printf("Error!");
      exit(1);
   }
   char c = fgetc(fptr);
   while (c != EOF){
        switch((char)c){
            case '>': if (condition != 2){
                if (pointer == 999){
                    printf("\nerror - out of range!");
                    exit(1);
                }
                pointer++;
                break;}
            case '<': if (condition != 2){
                if (pointer == 0){
                    printf("\nerror - out of range!");
                    exit(1);
                }
                pointer--;
                break;}
            case '+': if (condition != 2){
                memory[pointer]++;
                break;
            case '-':
                memory[pointer]--;
                break;}
            case '.': if (condition != 2){
                if ((int)memory[pointer] < 0 || (int)memory[pointer] > 177){
                    printf("\nerror - no Ascii value!");
                    exit(1);
                }
                printf("%c", memory[pointer]);
                break;}
            case '[':
                if (condition == 0 || condition == 1){ // i come across one when being free
                    if (memory[pointer] == 0){
                        condition = 2;
                    }
                    else {
                        condition = 1;
                    }
                }
                break;
            case ']':
                if (condition == 0){
                    printf("\nsyntax error!");
                    exit(1);
                }
                else if (condition == 1){
                    fseek(fptr, -2L, SEEK_CUR);
                    c = fgetc(fptr);
                    while (condition == 1){
                        if (c == '['){
                            condition = 0;
                            fseek(fptr, -1L, SEEK_CUR);
                            break;
                        }
                        fseek(fptr, -2L, SEEK_CUR);
                        c = fgetc(fptr);
                    }
                }
                else if (condition == 2){
                    condition = 0;
                }
                break;
        }
        c = fgetc(fptr);
   }
	return 0;
}
