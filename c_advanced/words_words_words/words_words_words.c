#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main()
{
    char* single_digit[] = {"", "one", "two", "three", "four", "five", "six", "seven", "eight", "nine"};
    char* up_to_twentie[] = {"ten", "eleven", "twelve", "thirteen", "fourteen", "fifteen", "sixteen", "seventeen", "eighteen", "nineteen"};
    char* two_digits[] = {"", "","twenty", "thirty", "forty", "fifty", "sixty", "seventy", "eighty", "ninety"};
    char* hundreds = "one hundred";
    size_t counter = 0;
    for (int i = 1; i < 101; i++){
        if (i < 10){
            counter += strlen(single_digit[i]);
        }
        else if (i < 20){
            counter += strlen(up_to_twentie[i - 10]);
        }
        else if (i < 100){
            int tens = i/10;
            counter += strlen(two_digits[tens]);
            int ones = i%10;
            counter += strlen(single_digit[ones]);
        }
        else{
            counter += strlen(hundreds);
        }
    }
    printf("%zu \n", counter);
    return(0);
}
