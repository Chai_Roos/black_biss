#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct pokemon{
    char name[20];
    float attack;
    float deffense;
    char effectiveness[5];
};

void fight(struct pokemon p1, struct pokemon p2){
    int p1_effective = 2; int p2_effective = 2;
    if (strcmp(p1.effectiveness, "water") == 0){
        if (strcmp(p2.effectiveness, "fire") == 0){
            p1_effective = 3; p2_effective = 1;
        }
        else if (strcmp(p2.effectiveness, "grass") == 0){
            p1_effective = 1; p2_effective = 3;
        }
    }
    else if (strcmp(p1.effectiveness, "fire") == 0){
        if (strcmp(p2.effectiveness, "water") == 0){
            p1_effective = 1; p2_effective = 3;
        }
        else if (strcmp(p2.effectiveness, "air") == 0){
            p1_effective = 3; p2_effective = 1;
        }
    }
    else if (strcmp(p1.effectiveness, "air") == 0){
        if (strcmp(p2.effectiveness, "fire") == 0){
            p1_effective = 1; p2_effective = 3;
        }
        else if (strcmp(p2.effectiveness, "grass") == 0){
            p1_effective = 3; p2_effective = 1;
        }
    }
    else if (strcmp(p1.effectiveness, "grass") == 0){
        if (strcmp(p2.effectiveness, "water") == 0){
            p1_effective = 3; p2_effective = 1;
        }
        else if (strcmp(p2.effectiveness, "air") == 0){
            p1_effective = 1; p2_effective = 3;
        }
    }
    double p1_strength = 50 * p1_effective * (p1.attack / p2.deffense);
    printf("%s : \n", p1.name);
    printf("effectiveness - %d \n", p1_effective);
    printf("power - %f \n\n", p1_strength);
    float p2_strength = 50 * p2_effective * (p2.attack / p1.deffense);
    printf("%s : \n", p2.name);
    printf("effectiveness - %d \n", p2_effective);
    printf("power - %f \n\n", p2_strength);
    printf("-------------------\n");
    if (p1_strength > p2_strength){
        printf("  %s wins! \n", p1.name);
    }
    else if (p1_strength < p2_strength){
        printf("  %s wins! \n", p2.name);
    }
    else {
        printf ("%s and %s are equally strong \n", p1.name, p2.name);
    }
    printf("-------------------");
}


int main(int argc, char * argv[])
{
    struct pokemon p1, p2;

    strcpy(p1.name, "Charizard"); p1.attack = 10.; p1.deffense = 4.; strcpy(p1.effectiveness, "fire");
    strcpy(p2.name, "Squirtle"); p2.attack = 7.; p2.deffense = 5.; strcpy(p2.effectiveness, "water");
    fight(p1, p2);
	return 0;
}
