#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct stick{
    char name;
};

void tower_of_the_biss(int num_of_disks, struct stick a, struct stick b, struct stick c){
    if (num_of_disks == 1){
        printf("Move disk from %c to %c\n", a.name, c.name);
    }
    else{
        tower_of_the_biss(num_of_disks - 1, a ,c ,b);
        tower_of_the_biss(1 ,a ,b ,c);
        tower_of_the_biss(num_of_disks - 1, b, a, c);
    }
}

int main()
{
    struct stick a, b, c;
    a.name = 'a'; b.name = 'b'; c.name = 'c';
    tower_of_the_biss(3, a, b, c);
    return(0);
}
