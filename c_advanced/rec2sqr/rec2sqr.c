#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct Rectangle {
    int height, width;
};

void actually_calculating(int height, int width){
    if (height == 0 || width == 0){
        printf("\n");
    }
    else if (height > width){
        printf("%d  ", width);
        actually_calculating(height - width, width);
    }
    else if (width > height){
        printf("%d  ", height);
        actually_calculating(height, width - height);
    }
    else if (width == height){
        printf("%d  ", width);
        actually_calculating(0, 0);
    }
}

void print_squares(struct Rectangle rec){
    actually_calculating(rec.height, rec.width);
}


int main(int argc, char * argv[])
{
    struct Rectangle my_rec;
	my_rec.height = 5;
	my_rec.width = 4;
    print_squares(my_rec);
	return 0;
}
